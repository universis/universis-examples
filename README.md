### universis-examples

[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

This project contains a set of examples based on Universis Project.

### [Bootstrap 4 Starter](https://gitlab.com/kbarbounakis/universis-examples/tree/master/projects/starter)

An Angular 6 application with Bootstrap 4 which uses core components and services of Universis project for displaying student data.

![Bootstrap 4 Starter Screenshot](https://gitlab.com/kbarbounakis/universis-examples/raw/master/projects/starter/screenshot.png?inline=false)
