import { Component, OnInit } from '@angular/core';
import { UserService } from '@universis/common';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  public user: any;

  constructor(private _userService: UserService) { }

  async ngOnInit() {
    this.user = await this._userService.getUser();
  }

}
